﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace holamundo.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult index()
		{
			ViewData ["message"] = this.message();
			return View();
		}

		public string message()
		{
			return "Hola desde aspx .net MVC.";
		}
	}
}

